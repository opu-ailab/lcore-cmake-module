set(LCORE_HOME_DIRECTORY $ENV{HOME}/shakehand)
set(LCORE_BINDIR ${LCORE_HOME_DIRECTORY}/bin)
set(LCORE_INCLUDE_DIR ${LCORE_HOME_DIRECTORY}/include)
set(LCORE_LIBDIR ${LCORE_HOME_DIRECTORY}/lib)
set(LCORE_SHAREDIR ${LCORE_HOME_DIRECTORY}/share)

function(install_lcore_bash_configs)
    set_directory_properties(PROPERTIES
        ADDITIONAL_MAKE_CLEAN_FILES "lcore-bash-config.bash;lcore-config.hpp;lcore-config.pm")

    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/lcore-bash-config.in)
        configure_file(lcore-bash-config.in lcore-bash-config.bash @ONLY)
        install(FILES lcore-bash-config.bash DESTINATION ${LCORE_LIBDIR}/lcore-bash-config.d
            RENAME ${PROJECT_NAME})
    endif()

    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/lcore-config.hpp.in)
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/lcore-config.hpp.in ${CMAKE_CURRENT_BINARY_DIR}/lcore-config.hpp @ONLY)
        install(FILES lcore-config.hpp DESTINATION ${LCORE_INCLUDE_DIR}/lcore/config
            RENAME ${PROJECT_NAME}.hpp)
    endif()

    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/lcore-config.pm.in)
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/lcore-config.pm.in ${CMAKE_CURRENT_BINARY_DIR}/lcore-config.pm @ONLY)
        install(FILES lcore-config.pm DESTINATION ${LCORE_LIBDIR}/perl/LCore/Config
            RENAME ${PROJECT_NAME}.pm)
    endif()
endfunction()
